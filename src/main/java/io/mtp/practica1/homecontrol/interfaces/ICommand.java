package io.mtp.practica1.homecontrol.interfaces;

@FunctionalInterface
public interface ICommand {
    public void execute();
}
