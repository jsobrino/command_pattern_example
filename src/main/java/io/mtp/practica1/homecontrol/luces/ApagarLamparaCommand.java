package io.mtp.practica1.homecontrol.luces;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class ApagarLamparaCommand implements ICommand {
    Lampara lampara;

    public ApagarLamparaCommand(Lampara lampara){
        super();
        this.lampara = lampara;
    }

    public void execute(){
        System.out.println("Apagando lámpara...");
        lampara.apagar();
    }
}
