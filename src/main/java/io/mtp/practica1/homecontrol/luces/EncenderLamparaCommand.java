package io.mtp.practica1.homecontrol.luces;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class EncenderLamparaCommand implements ICommand {
    Lampara lampara;

    public EncenderLamparaCommand(Lampara lampara){
        super();
        this.lampara = lampara;
    }

    public void execute(){
        System.out.println("Encendiendo lampara...");
        lampara.encender();
    }
}
