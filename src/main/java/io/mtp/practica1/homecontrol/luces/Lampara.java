package io.mtp.practica1.homecontrol.luces;

public class Lampara {
    public void encender(){
        System.out.println("Se enciende la luz");
    }

    public void apagar(){
        System.out.println("Se apaga la luz");
    }
}
