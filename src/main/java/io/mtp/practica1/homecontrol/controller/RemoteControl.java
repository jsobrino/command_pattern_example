package io.mtp.practica1.homecontrol.controller;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class RemoteControl {
    ICommand command;

    public void setCommand(ICommand command){
        this.command = command;
    }

    public void botonPulsado(){
        command.execute();
    }
}
