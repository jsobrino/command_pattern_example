package io.mtp.practica1.homecontrol.ventilador;

public class Ventilador {
    public void start(){
        System.out.println("Se enciende el ventilador");
    }

    public void stop(){
        System.out.println("Se apaga el ventilador");
    }
}
