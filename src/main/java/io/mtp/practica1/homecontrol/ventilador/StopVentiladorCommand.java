package io.mtp.practica1.homecontrol.ventilador;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class StopVentiladorCommand implements ICommand {
    Ventilador ventilador;

    public StopVentiladorCommand(Ventilador ventilador){
        super();
        this.ventilador = ventilador;
    }

    public void execute(){
        System.out.println("Apagando ventilador...");
        ventilador.stop();
    }

}
