package io.mtp.practica1.homecontrol.ventilador;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class StartVentidladorCommand implements ICommand {

    Ventilador ventilador;

    public StartVentidladorCommand(Ventilador ventilador){
        super();
        this.ventilador = ventilador;
    }

    public void execute(){
        System.out.println("Encendiendo ventilador...");
        ventilador.start();
    }

}
