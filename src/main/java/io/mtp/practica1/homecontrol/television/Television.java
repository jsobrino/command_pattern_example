package io.mtp.practica1.homecontrol.television;

public class Television {
    public void turnOn(){
        System.out.println("Se enciende la TV");
    }

    public void turnOff(){
        System.out.println("Se apaga la TV");
    }
}
