package io.mtp.practica1.homecontrol.television;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class EncenderTelevisionCommand implements ICommand{
    Television television;

    public EncenderTelevisionCommand(Television television){
        super();
        this.television = television;
    }

    public void execute(){
        System.out.println("Encendiendo la televisión...");
        television.turnOn();
    }
}
