package io.mtp.practica1.homecontrol.television;

import io.mtp.practica1.homecontrol.interfaces.ICommand;

public class ApagarTelevisionCommand implements ICommand {
    Television television;

    public ApagarTelevisionCommand(Television television){
        super();
        this.television = television;
    }

    public void execute(){
        System.out.println("Apagando la televisión...");
        television.turnOff();
    }
}
