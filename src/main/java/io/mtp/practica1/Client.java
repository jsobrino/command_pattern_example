package io.mtp.practica1;

import io.mtp.practica1.homecontrol.controller.RemoteControl;
import io.mtp.practica1.homecontrol.luces.ApagarLamparaCommand;
import io.mtp.practica1.homecontrol.luces.EncenderLamparaCommand;
import io.mtp.practica1.homecontrol.luces.Lampara;
import io.mtp.practica1.homecontrol.television.EncenderTelevisionCommand;
import io.mtp.practica1.homecontrol.television.Television;
import io.mtp.practica1.homecontrol.ventilador.StopVentiladorCommand;
import io.mtp.practica1.homecontrol.ventilador.Ventilador;

public class Client {
    public static void main (String[] args){
        //Receivers
        Lampara nuevaLampara = new Lampara();
        Ventilador nuevoVentilador = new Ventilador();
        Television nuevaTelevision = new Television();
        Lampara otraLampara = new Lampara();

        //Invoker
        RemoteControl mando = new RemoteControl();

        mando.setCommand(new EncenderLamparaCommand(nuevaLampara));
        mando.botonPulsado();

        mando.setCommand(new ApagarLamparaCommand(nuevaLampara));
        mando.botonPulsado();

        mando.setCommand(new StopVentiladorCommand(nuevoVentilador));
        mando.botonPulsado();

        mando.setCommand(new EncenderTelevisionCommand(nuevaTelevision));
        mando.botonPulsado();

        mando.setCommand(new ApagarLamparaCommand(otraLampara));
        mando.botonPulsado();
    }
}
